-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 25 mei 2018 om 13:20
-- Serverversie: 5.6.34-log
-- PHP-versie: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trainingfactory`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `deelnemers`
--

CREATE TABLE `deelnemers` (
  `user_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `deelnemers`
--

INSERT INTO `deelnemers` (`user_id`, `lesson_id`) VALUES
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `lessons`
--

CREATE TABLE `lessons` (
  `id` int(11) NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max_persons` int(11) NOT NULL,
  `training_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `lessons`
--

INSERT INTO `lessons` (`id`, `time`, `date`, `location`, `max_persons`, `training_id`) VALUES
(1, '18:00:00', '2018-05-17', '1', 10, 1),
(2, '16:00:00', '2018-05-30', '3', 4, 4),
(3, '11:00:00', '2019-04-08', '2', 6, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `trainingen`
--

CREATE TABLE `trainingen` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `trainingen`
--

INSERT INTO `trainingen` (`id`, `name`, `description`, `duration`, `photo`) VALUES
(1, 'MMA', 'Mixed martial arts, meestal afgekort tot MMA, is een multidisciplinaire vechtsport die zich richt op het combineren van technieken uit verschillende vechtkunsten zoals worstelen, judo, karate, kungfu, kickboksen, thaiboksen, boksen en jiujitsu.', 120, 'MMA.jpg'),
(2, 'Kickboksen', 'Kickboksen is een vechtsport waarbij zowel de handen als de benen mogen worden gebruikt. De sport kent zijn oorsprong in Japan en de Verenigde Staten, waar het begin jaren zeventig populair werd.', 60, 'kickboksen.jpeg'),
(3, 'Stootzak training', 'Stootzak training is een les waarbij gebruik wordt gemaakt van de stootzak. Op de stootzakken worden verschillende klassieke boks en traptechnieken', 60, 'stootzaktraining.jpg'),
(4, 'jiujitsu', 'Jiujitsu is in principe geen sport, hoewel er wel een sportieve versie van bestaat die vaak ook kortweg jiujitsu genoemd wordt, dat het fighting system, groundfight, duo system en Random Attacks kent. Een vechtsport is gebonden aan regels, jiujitsu daaren', 90, 'jiujitsu.jpg');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `preprovision` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `dateofbirth` date NOT NULL,
  `hiring_date` date DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `street` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `postalcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `gender` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `preprovision`, `lastname`, `dateofbirth`, `hiring_date`, `salary`, `street`, `postalcode`, `place`, `roles`, `gender`) VALUES
(1, 'yuri', '$2y$13$51W7KXgIqTLLRxiC4AhPgeN.fKefTbvA1qQ.g8hoYKMJx5A5LdXTy', 'Yuri', NULL, 'Lamijo', '1999-04-08', '2018-04-08', 5000.00, 'Spieringsweteringweg 14', '2286 KG', 'Rijswijk', '[\"ROLE_ADMIN\"]', 'Man'),
(2, 'peter', '$2y$13$huDhyp8Thc9GAt20gnd61OQg4o4qkWfV82o/sqmtk3PCWsYZ/j1QO', 'Peter', NULL, 'Born', '1999-04-11', NULL, NULL, 'Rijswijkselandingslaan 245', '2246 FH', 'Nootdorp', '[\"ROLE_LID\"]', 'Man'),
(3, 'koen', '$2y$13$Itu6CEMuomkZYCrfI/uGDO/17UP5jwSQokaBQdzIJCMAW/8pABETi', 'Koen', NULL, 'Kort', '1999-04-25', NULL, NULL, 'Straat', '3245 JD', 'Poeldijk', '[\"ROLE_LID\"]', 'Man'),
(5, 'jan', '$2y$13$v85HoZGdfhPKndKC4MV5beseKubNwoaWl1w8W4QLJfmuluvk1Hf5i', 'Jan', 'van', 'boeken', '1996-07-16', NULL, NULL, 'Boekenlaan 63', '1342 JD', 'Rotterdam', '[\"ROLE_LID\"]', 'Man'),
(6, 'jasper', '$2y$13$baSYwXZCKQnamERiUL2wVetBvN54701NV36IoISAL9m4czWpsXuNe', 'Jasper', 'van', 'Lagen', '1998-06-05', NULL, NULL, 'Lagenhogen 3', '3212 JF', 'Den Haag', '[\"ROLE_LID\"]', 'Man'),
(7, 'sem', '$2y$13$RNpR9s91iVVj.rVQ6vGnBuDLF6wvbYLklbPd6uQRIOXjSvciRo/v6', 'Sem', NULL, 'Schilt', '1998-04-06', '2018-05-25', 4000.00, 'Lagenhogen 3', '2312 BB', 'Poeldijk', '[\"ROLE_INSTRUCTEUR\"]', 'Man'),
(8, 'jeppe', '$2y$13$9zdjWoY8WDaZ979g7ZHSJOFyJjjFC1.5OzAv1oRwtF.wzxVjs3/42', 'Jeppe', NULL, 'Born', '1999-04-17', '2018-01-16', 3000.00, 'Rijswijkselandingslaan 245', '2312 BB', 'Nootdorp', '[\"ROLE_INSTRUCTEUR\"]', 'Man');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `deelnemers`
--
ALTER TABLE `deelnemers`
  ADD PRIMARY KEY (`user_id`,`lesson_id`),
  ADD KEY `IDX_C9160B29A76ED395` (`user_id`),
  ADD KEY `IDX_C9160B29CDF80196` (`lesson_id`);

--
-- Indexen voor tabel `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3F4218D9BEFD98D1` (`training_id`);

--
-- Indexen voor tabel `trainingen`
--
ALTER TABLE `trainingen`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT voor een tabel `trainingen`
--
ALTER TABLE `trainingen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `deelnemers`
--
ALTER TABLE `deelnemers`
  ADD CONSTRAINT `FK_C9160B29A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C9160B29CDF80196` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE;

--
-- Beperkingen voor tabel `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `FK_3F4218D9BEFD98D1` FOREIGN KEY (`training_id`) REFERENCES `trainingen` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
