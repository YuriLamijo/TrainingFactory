<?php
/**
 * Created by PhpStorm.
 * User: Yuri Lamijo
 * Date: 14-5-2018
 * Time: 15:26
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lessons")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LessonRepository")
 */
class Lesson
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="time")
     * @Assert\NotBlank(message="Vul een tijd in")
     */
    private $time;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Vul een datum in")
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Vul een locatie in")
     */
    private $location;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Vul een max aantal deelnemers in")
     */
    private $max_persons;

    /**
     * @ORM\ManyToOne(targetEntity="Training", inversedBy="lesson")
     * @ORM\JoinColumn(name="training_id", referencedColumnName="id")
     */
    private $training;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="lesson")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getMaxPersons()
    {
        return $this->max_persons;
    }

    public function setMaxPersons($max_persons)
    {
        $this->max_persons = $max_persons;
    }

    public function getTraining()
    {
        return $this->training;
    }

    public function setTraining($training)
    {
        $this->training = $training;
    }


}