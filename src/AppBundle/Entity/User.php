<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 20-4-2018
 * Time: 10:09
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     * @Assert\NotBlank(message="Vul een gebruikersnaam in")
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=65)
     */
    private $password;

    /**
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(message="Vul een naam in")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $preprovision;

    /**
     * @ORM\Column(type="string", length=35)
     * @Assert\NotBlank(message="Vul een achternaam in")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(message="Selecteer een geslacht")
     */
    private $gender;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Vul een geboortedatum in")
     */
    private $dateofbirth;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $hiring_date;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $salary;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Vul een straat in")
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="Vul een postcode in")
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", length=35)
     * @Assert\NotBlank(message="Vul een plaats in")
     */
    private $place;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = array();

    /**
     * @ORM\ManyToMany(targetEntity="Lesson", inversedBy="users")
     * @ORM\JoinTable(name="deelnemers")
     */
    private $lesson;

    public function __construct()
    {
        $this->lesson = new ArrayCollection();
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {

    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
        ]);
    }


    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }

    public function addLesson(Lesson $l){
        if($this->lesson->contains($l)){
            return;
        }
        $this->lesson->add($l);
    }

    public function removeLesson(Lesson $l){
        if(!$this->lesson->contains($l)){
            return;
        }
        $this->lesson->removeElement($l);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }


    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getPreprovision()
    {
        return $this->preprovision;
    }

    public function setPreprovision($preprovision)
    {
        $this->preprovision = $preprovision;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getLesson()
    {
        return $this->lesson;
    }

    public function setLesson($lesson)
    {
        $this->lesson = $lesson;
    }

    public function getDateofbirth()
    {
        return $this->dateofbirth;
    }

    public function setDateofbirth($dateofbirth)
    {
        $this->dateofbirth = $dateofbirth;
    }

    public function getHiringDate()
    {
        return $this->hiring_date;
    }

    public function setHiringDate($hiring_date)
    {
        $this->hiring_date = $hiring_date;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet($street)
    {
        $this->street = $street;
    }

    public function getPostalcode()
    {
        return $this->postalcode;
    }

    public function setPostalcode($postalcode)
    {
        $this->postalcode = $postalcode;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace($place)
    {
        $this->place = $place;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }


}