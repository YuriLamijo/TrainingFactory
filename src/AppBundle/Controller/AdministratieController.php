<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 20-4-2018
 * Time: 12:14
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Training;
use AppBundle\Entity\User;
use AppBundle\Form\UserForm;
use AppBundle\Form\TrainingForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdministratieController extends Controller
{
    /**
     * @Route("/admin/lessen", name="lessenoverzicht")
     */
    public function lessenAction()
    {
        return $this->render("admin/index.html.twig", [

        ]);
    }

    /*
     * Leden Functions
     */

    /**
     * @Route("/admin/ledenbeheer", name="ledenbeheer")
     */
    public function ledenBeheerAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        $leden = array_filter($users, function ($user) {
            return in_array("ROLE_LID", $user->getRoles());
        });

        return $this->render("admin/leden/ledenBeheer.html.twig", [
            'leden' => $leden
        ]);
    }

    /**
     * @Route("/admin/lidwijzigen/{id}", name="lidwijzigen")
     */
    public function ledenwijzigAction($id, Request $request)
    {
        $a = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);

        $form = $this->createForm(UserForm::class, $a);
        $form
            ->add('save', SubmitType::class, array('label' => "aanpassen"))
            ->remove('plainPassword')->remove('hiring_date')->remove('salary');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($a);

            $em->flush();
            $this->addFlash(
                'notice',
                'activiteit aangepast!'
            );
            return $this->redirectToRoute('ledenbeheer');
        }


        return $this->render("admin/leden/ledenWijzigen.html.twig", [
            'ledenForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/ledenbeheer/lidverwijderen/{id}", name="lidverwijderen")
     */
    public function ledenVerwijderen(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $lid = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        $em->remove($lid);
        $em->flush();

        $this->addFlash(
            'success',
            'lid is verwijderd'
        );
        return $this->redirectToRoute('ledenbeheer');
    }

    /*
     * Training functions
     */

    /**
     * @Route("/admin/trainingbeheer", name="trainingbeheer")
     */
    public function trainingBeheerAction()
    {
        $trainingen = $this->getDoctrine()->getRepository(Training::class)->findAll();

        return $this->render("admin/training/trainingBeheer.html.twig", [
            'trainingen' => $trainingen
        ]);
    }

    /**
     * @Route("/admin/trainingbeheer/toevoegen", name="trainingtoevoegen")
     */
    public function trainingToevoegen(Request $request)
    {

        $training = new Training();
        $form = $this->createForm(TrainingForm::class, $training);
        $form->add('save', SubmitType::class, ['label' => "Toevoegen"]);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($training);
            $em->flush();
            $this->addFlash(
                'success',
                'De training is toegevoegd.'
            );

            return $this->redirectToRoute('trainingbeheer');
        }

        return $this->render("admin/training/trainingToevoegen.html.twig", [
            'trainingForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/trainingbeheer/wijzigen/{id}", name="trainingwijzigen")
     */
    public function trainingWijzigen(Request $request, $id)
    {
        $training = $this->getDoctrine()
            ->getRepository(Training::class)
            ->find($id);

        $form = $this->createForm(TrainingForm::class, $training);
        $form->add('save', SubmitType::class, ['label' => "wijzigen"]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($training);
            $em->flush();

            $this->addFlash(
                'success',
                'De training is gewijziged'
            );

            return $this->redirectToRoute('trainingbeheer');
        }

        return $this->render("admin/training/trainingWijzigen.html.twig", [
            'trainingForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/trainingbeheer/verijderen/{id}", name="trainingverwijderen")
     */
    public function trainingVerwijderen(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $training = $this->getDoctrine()
            ->getRepository(Training::class)
            ->find($id);
        $em->remove($training);
        $em->flush();

        $this->addFlash(
            'success',
            'Training is verwijderd'
        );
        return $this->redirectToRoute('trainingbeheer');
    }

    /*
     * Insructeur functions
     */

    /**
     * @Route("/admin/instructeurbeheer", name="instructeurbeheer")
     */
    public function instructeurBeheer()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        $instructeur = array_filter($users, function ($user) {
            return in_array("ROLE_INSTRUCTEUR", $user->getRoles());
        });

        return $this->render("admin/instructeur/instructeurBeheer.html.twig", [
            'instructeurs' => $instructeur
        ]);
    }

    /**
     * @Route("/admin/instructeurwijzigen/{id}", name="instructeurwijzigen")
     */
    public function instructeurwijzigen(Request $request, $id)
    {
        $instructeur = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        $form = $this->createForm(UserForm::class, $instructeur);
        $form->add('save', SubmitType::class, array('label' => "wijzigen"))
            ->remove('plainPassword');

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($instructeur);
            $em->flush();
            $this->addFlash(
                'success',
                'De instructeur is gewijziged'
            );

            return $this->redirectToRoute('instructeurbeheer');
        }

        return $this->render("admin/instructeur/instructeurWijzigen.html.twig", [
            'instructeurForm'=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/instructeurtoevoegen", name="instructeurtoevoegen")
     */

    public function instructeurtoevoegen(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $instructeur = new User();

        $form = $this->createForm(UserForm::class, $instructeur);
        $form->add('save', SubmitType::class, ['label' => "Toevoegen"]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $password = $passwordEncoder->encodePassword($instructeur, $instructeur->getPlainPassword());
            $instructeur->setPassword($password);
            $instructeur->setRoles(['ROLE_INSTRUCTEUR']);
            $em->persist($instructeur);
            $em->flush();
            $this->addFlash(
                'success',
                'De instructeur is toegevoegd.'
            );

            return $this->redirectToRoute('instructeurbeheer');
        }
        return $this->render("admin/instructeur/instructeurToevoegen.html.twig", [
            'InstructeurForm' => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/instructeurverwijderen/{id}", name="instructeurverwijderen")
     */
    public function instructeurverwijderen($id)
    {
        $em = $this->getDoctrine()->getManager();
        $instructeur = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        $em->remove($instructeur);
        $em->flush();

        $this->addFlash(
            'success',
            'Instructeur is verwijderd'
        );
        return $this->redirectToRoute('instructeurbeheer');
    }
}