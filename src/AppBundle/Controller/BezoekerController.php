<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Training;
use AppBundle\Entity\User;
use AppBundle\Form\UserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class BezoekerController extends Controller {
    /**
     * @Route("/", name="homepage")
     */
    public function BezoekerAction(Request $request){
        return $this->render("bezoeker/index.html.twig", [

        ]);
    }

    /**
     * @Route("/aanbod", name="aanbod")
     */
    public function AanbodAction(){

        $trainingen = $this->getDoctrine()->getRepository(Training::class)->findAll();

        return $this->render('bezoeker/aanbod.html.twig', [
            'trainingen'=>$trainingen
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function ContactAanbod(){
        return $this->render('bezoeker/contact.html.twig', [

        ]);
    }

    /**
     * @Route("/regels", name="regels")
     */
    public function RegelsAanbod(){
        return $this->render('bezoeker/regels.html.twig', [

        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function LoginAanbod(Request $request, AuthenticationUtils $authUtils){
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('bezoeker/login.html.twig', [

        ]);
    }

    /**
     * @Route("/registeren", name="registeren")
     */
    public function RegisterenAanbod(Request $request, UserPasswordEncoderInterface $passwordEncoder){
        $lid = new User();
        $form = $this->createForm(UserForm::class, $lid);
        $form->remove('hiring_date')->remove('salary');

        $form->handleRequest($request);
      
        if($form->isSubmitted() && $form->isValid()){
            $password = $passwordEncoder->encodePassword($lid, $lid->getPlainPassword());
            $lid->setPassword($password);
            $lid->setRoles(['ROLE_LID']);

            $em = $this->getDoctrine()->getManager();

            $em->persist($lid);
            $em->flush();
            $this->addFlash(
                'success',
                'U bent ingeschreven'
            );
            return $this->redirectToRoute('homepage');
        }

        return $this->render('bezoeker/registeren.html.twig', [
            'userForm'=>$form->createView()
        ]);
    }
}