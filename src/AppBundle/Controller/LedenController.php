<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 20-4-2018
 * Time: 12:15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Lesson;
use AppBundle\Entity\User;
use AppBundle\Form\UserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class LedenController extends Controller
{
    /**
     * @Route("/lid/home", name="homeLid")
     */
    public function LidAction (){
        return $this->render("lid/index.html.twig", [
        ]);
    }

    /**
     * @Route("/lid/lesaanbod", name="lesaanbod")
     */
    public function LesAanbodAction(){
        $userid = $this->get("security.token_storage")->getToken()->getUser();

        $beschikbareLessons = $this->getDoctrine()
            ->getRepository(Lesson::class)
            ->getBeschikbareLessons($userid);


        $IngeschrevenLessons = $this->getDoctrine()
            ->getRepository(Lesson::class)
            ->getIngeschrevenLessons($userid);

        return $this->render("lid/inschrijven.html.twig", [
            'beschikbareLessons' => $beschikbareLessons,
            'IngeschrevenLessons' => $IngeschrevenLessons
        ]);
    }

    /**
     * @Route("/lid/lesaanbod/inschrijven/{id}", name="inschrijven")
     */
    public function inschrijvenAction($id) {
        $les = $this->getDoctrine()
            ->getRepository(Lesson::class)
            ->find($id);
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $usr->addLesson($les);

        $em = $this->getDoctrine()->getManager();
        $em->persist($usr);
        $em->flush();

        return $this->redirectToRoute('lesaanbod');
    }

    /**
     * @Route("/lid/lesaanbod/uitchrijven/{id}", name="uitschrijven")
     */
    public function uitschrijvenAction($id) {
        $les = $this->getDoctrine()
            ->getRepository(Lesson::class)
            ->find($id);
        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $usr->removeLesson($les);

        $em = $this->getDoctrine()->getManager();
        $em->persist($usr);
        $em->flush();

        return $this->redirectToRoute('lesaanbod');
    }
    /**
     * @Route("/lid/lidwijzigen", name="lidwijzigen")
     */
    public function lidwijzigen(Request $request) {
        $user = $this->get("security.token_storage")->getToken()->getUser();
        $form = $this->createForm(UserForm::class,$user);
        $form->add('submit', SubmitType::class);
        $form->remove('plainPassword')
            ->remove('hiring_date')
            ->remove('salary');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                'success',
                'De lid is gewijziged'
            );
            return $this->redirectToRoute('homeLid');
        }
        return $this->render('lid/lidWijzigen.html.twig',[
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/lid/wachtwoordwijzigen", name="wachtwoordwijzigen")
     */
    public function wachtwoordwijzigen(Request $request) {
        $user = $this->get("security.token_storage")->getToken()->getUser();
        $form = $this->createFormBuilder($user)
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Wachtwoord'],
                'second_options' => ['label' =>'Herhaal']
            ))
            ->add('submit', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();
            $this->addFlash(
                'success',
                'De lid is gewijziged'
            );
            return $this->redirectToRoute('homeLid');
        }
        return $this->render('lid/lidwijzigen.html.twig', [
            'form' => $form->createView()
        ]);
    }
}