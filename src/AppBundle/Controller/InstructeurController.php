<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 20-4-2018
 * Time: 12:15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Lesson;
use AppBundle\Entity\User;
use AppBundle\Form\LesForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class InstructeurController extends Controller
{
    /**
     * @Route("instructeur/home", name="homeInstructeur")
     */
    public function InstructeurAction() {
        return $this->render("instructeur/index.html.twig", [

        ]);
    }

    /**
     * @Route("instructeur/lessenbeheer", name="lessenbeheer")
     */
    public function lessenBeheer() {
        $lesson = $this->getDoctrine()->getRepository(Lesson::class)
            ->findAll();
        return $this->render("instructeur/lessenbeheer.html.twig", [
            'lesson'=>$lesson
        ]);
    }

    /**
     * @Route("instructeur/lesbeheer/wijzigen/{id}", name="leswijzigen")
     */
    public function leswijzigen(Request $request, $id) {
        $lesson = $this->getDoctrine()->getRepository(Lesson::class)
            ->find($id);
        $form = $this->createForm(LesForm::class, $lesson);
        $form ->add('submit', SubmitType::class);
        $form ->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em -> persist($lesson);
            $em -> flush();

            $this->addFlash("success", "les is gewijzig!, hoera");

            return $this->redirectToRoute("lessenbeheer");
        }

        return $this->render("instructeur/leswijzigen.html.twig", [
            'lesform' => $form->createView()
        ]);
    }

    /**
     * @Route("instructeur/lesbeheer/toevoegen", name="lestoevoegen")
     */
    public function lesToevoegen(Request $request) {
        $les = new Lesson();
        $form = $this->createForm(LesForm::class, $les);
        $form -> add('Submit', SubmitType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($les);
            $em->flush();

            return $this->redirectToRoute("lessenbeheer");
        }
        return $this->render('instructeur/lestoevoegen.html.twig', [
            'lesform' => $form->createView()
        ]);
    }

    /**
     * @Route("instructeur/lesbeheer/verwijderen/{id}", name="lesverwijderen")
     */
    public function lesVerwijderen($id) {

        $em = $this->getDoctrine()->getManager();
        $les = $this->getDoctrine()->getRepository(Lesson::class)
            ->find($id);
        $em->remove($les);
        $em->flush();
        return $this->redirectToRoute('lessenbeheer');
    }

    /**
     * @Route("instructeur/lesbeheer/deelnemerslijst/{id}", name="deelnemerslijst")
     */
    public function deelnemerslijst($id){

        $deelnemers = $this->getDoctrine()
            ->getRepository(User::class)
            ->getDeelnemers($id);

        $les = $this->getDoctrine()
            ->getRepository(Lesson::class)
            ->find($id);

        return $this->render('instructeur/deelnemerslijst.html.twig', [
            'deelnemers' => $deelnemers,
            'les' => $les
        ]);
    }
}