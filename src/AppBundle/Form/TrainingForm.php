<?php
/**
 * Created by PhpStorm.
 * User: Yuri Lamijo
 * Date: 22-5-2018
 * Time: 09:23
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrainingForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Naam'])
            ->add('description', TextType::class, ['label' => 'Omschrijving'])
            ->add('price', NumberType::class, ['scale' => 2, 'label' => 'prijs'])
            ->add('duration', TextType::class, ['label' => 'Duur'])
            ->add('photo', TextType::class, ['label' => 'Foto'])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => 'AppBundle\Entity\Training'
        ]);
    }

}