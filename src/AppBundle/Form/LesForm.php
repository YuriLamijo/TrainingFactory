<?php
/**
 * Created by PhpStorm.
 * User: pjaiter
 * Date: 24-5-2018
 * Time: 11:16
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

class LesForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
           ->add('training', EntityType::class, [
               'class' => 'AppBundle:Training',
               'choice_label' => 'name',
               'label' => 'Training'
           ])
           ->add('max_persons', IntegerType::class, ['label' => 'Max personen'])
           ->add('date',DateType::class,[
               'widget' => 'choice',
               'format' => 'ddMMyyyy',
               'years' => range(2018,1950),
               'label' => 'Datum'
           ])
           ->add('time',TimeType::class,[
               'input' => 'datetime',
               'widget' => 'choice',
               'label' => 'Tijd'
               ])
           ->add('location', TextType::class, ['label' => 'Locatie'])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Lesson'
        ]);
    }

}