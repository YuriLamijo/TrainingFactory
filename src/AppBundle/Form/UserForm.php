<?php
/**
 * Created by PhpStorm.
 * User: Yuri Lamijo
 * Date: 14-5-2018
 * Time: 11:52
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'Gebruikersnaam'])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Wachtwoord'],
                'second_options' => ['label' =>'Herhaal']
            ])
            ->add('firstname', TextType::class, ['label' => 'Voornaam'])
            ->add('preprovision', TextType::class, ['label' => 'Tussenvoegsel'])
            ->add('lastname', TextType::class, ['label' => 'Achternaam'])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Man' => 'Man',
                    'Vrouw' => 'Vrouw'
                ],
                'label' => 'Geslacht'
            ])
            ->add('dateofbirth', DateType::class, [
                'widget' => 'choice',
                'format' => 'ddMMyyyy',
                'years' => range(2018,1950),
                'label' => 'Geboorte datum'
            ])
            ->add('hiring_date', DateType::class, [
                'widget' => 'choice',
                'format' => 'ddMMyyyy',
                'years' => range(2018,1950),
                'label' => 'Start datum'
            ])
            ->add('salary', IntegerType::class, ['label' => 'Salaris'])
            ->add('street', TextType::class, ['label' => 'Straat'])
            ->add('postalcode', TextType::class, ['label' => 'Postcode'])
            ->add('place', TextType::class, ['label' => 'Plaats'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User'
        ]);
    }
}