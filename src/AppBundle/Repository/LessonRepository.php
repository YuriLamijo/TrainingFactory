<?php
/**
 * Created by PhpStorm.
 * User: Yuri Lamijo
 * Date: 14-5-2018
 * Time: 15:55
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class LessonRepository extends EntityRepository
{
    public function getBeschikbareLessons($userid){
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT l FROM AppBundle:Lesson l WHERE :userid NOT MEMBER OF l.users");

        $query->setParameter('userid', $userid);

        return $query->getResult();
    }

    public function getIngeschrevenLessons($userid){
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT l FROM AppBundle:Lesson l WHERE :userid MEMBER OF l.users");

        $query->setParameter('userid', $userid);

        return $query->getResult();
    }
}