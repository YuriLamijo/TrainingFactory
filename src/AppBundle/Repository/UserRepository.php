<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 20-4-2018
 * Time: 12:11
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
//    public function getInstructeurs($afd)
//    {
//        $em = $this->getEntityManager();
//
//        $query = $em->createQuery('SELECT u FROM AppBundle:User u  WHERE u.roles =: `["ROLE_INSTRUCTEUR"]` ');
//
//        $query->setParameter('roles', $afd);
//        return $query->getResult();
//    }

//    public function getLeden()
//    {
//        $em = $this->getEntityManager();
//        $query = $em->createQuery('SELECT u FROM AppBundle:User u WHERE u.roles = 0)');
//        return $query->getResult();
//    }
    public function getDeelnemers($les){
        $em = $this->getEntityManager();

        $repo = $em->getRepository(User::class);

        $query = $repo->createQueryBuilder('u')
            ->innerJoin('u.lesson', 'l')
            ->where('l.id = :lesson_id')
            ->setParameter('lesson_id', $les)
            ->getQuery()->getResult();
        return $query;
    }
}