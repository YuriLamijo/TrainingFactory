<?php
/**
 * Created by PhpStorm.
 * User: Yuri
 * Date: 20-4-2018
 * Time: 12:58
 */

namespace AppBundle\Handler;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var RouterInterface $router
     */
    protected $router;

    /**
     * @var AuthorizationCheckerInterface $authorizationChecker
     */
    protected $authorizationChecker;

    public function __construct(RouterInterface $router, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param   Request         $request
     * @param   TokenInterface  $token
     *
     * @return Response never null
     */

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if($this->authorizationChecker->isGranted('ROLE_ADMIN')){
            $response = new RedirectResponse($this->router->generate('lessenoverzicht'));
        } elseif ($this->authorizationChecker->isGranted('ROLE_LID')){
            $response = new RedirectResponse($this->router->generate('homeLid'));
        } elseif ($this->authorizationChecker->isGranted('ROLE_INSTRUCTEUR')){
            $response = new RedirectResponse($this->router->generate('homeInstructeur'));
        }
        return $response;
    }
}